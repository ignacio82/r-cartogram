---
title: "Cartogram"
author: "ignacio"
date: "4/22/2015"
output: html_document
---

```{r, , comment = NA, results = "asis", comment = NA, tidy=F, message=FALSE, warning=FALSE}

DF_countries <- readRDS(file = './data/DF_countries.RDS')
library(rCharts)
library(pipeR)
library(rlist)
library(dplyr)

carto <- rCharts$new()
carto$setLib(".")
carto$lib = "carto"
carto$LIB$name = "carto"
carto$set(
  map = "worldcountries.topojson"  #url to the topojson source
)

carto$setTemplate(
  chartDiv = "
  <{{container}} id = '{{ chartId }}' class = '{{ lib }}' style='height:100%;width:100%;'>
  <div id='map-container'>
  <!--img id='placeholder' alt='placeholder image for old browsers' src='placeholder.png'-->
  <svg id = 'map' style = 'height:100%; width:100%' viewbox = '0 0 {{params.width}} {{params.height}}'>
  </svg>
  </div>
  </{{ container}}>" 
  , script = "./layouts/chart.html"
)
# now let's see if we can supply this data to our cartogram
carto$set(
  data = DF_countries
  , x = "n"
  #, x = "rr"
)
carto$show('iframesrc', cdn = TRUE)
```
